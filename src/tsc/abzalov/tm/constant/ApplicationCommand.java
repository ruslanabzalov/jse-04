package tsc.abzalov.tm.constant;

/**
 * Класс, содержащий константы - команды приложения.
 * @author Ruslan Abzalov.
 */
public class ApplicationCommand {

    public static final String CMD_HELP = "help";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";

}